#!/bin/bash

cat reserve/product/app/YouTube/YouTube.apk.* 2>/dev/null >> reserve/product/app/YouTube/YouTube.apk
rm -f reserve/product/app/YouTube/YouTube.apk.* 2>/dev/null
cat reserve/product/app/Maps/Maps.apk.* 2>/dev/null >> reserve/product/app/Maps/Maps.apk
rm -f reserve/product/app/Maps/Maps.apk.* 2>/dev/null
cat reserve/product/app/Gmail2/Gmail2.apk.* 2>/dev/null >> reserve/product/app/Gmail2/Gmail2.apk
rm -f reserve/product/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat reserve/product/app/Photos/Photos.apk.* 2>/dev/null >> reserve/product/app/Photos/Photos.apk
rm -f reserve/product/app/Photos/Photos.apk.* 2>/dev/null
cat system/system/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null >> system/system/priv-app/OnePlusCamera/OnePlusCamera.apk
rm -f system/system/priv-app/OnePlusCamera/OnePlusCamera.apk.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> system/system/product/priv-app/GmsCore/GmsCore.apk
rm -f system/system/product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> system/system/product/priv-app/Velvet/Velvet.apk
rm -f system/system/product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> system/system/product/app/WebViewGoogle/WebViewGoogle.apk
rm -f system/system/product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f system/system/product/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f system/system/product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
