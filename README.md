## qssi-user 11 RKQ1.201217.002 2109171635 release-keys
- Manufacturer: oneplus
- Platform: sdm845
- Codename: OnePlus6
- Brand: OnePlus
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.201217.002
- Incremental: 2109171635
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus6/OnePlus6:11/RKQ1.201217.002/2109171635:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.201217.002-2109171635-release-keys
- Repo: oneplus_oneplus6_dump_26208


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
